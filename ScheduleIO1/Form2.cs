﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using MySql.Data.MySqlClient;
using Tutorial.SqlConn;

namespace ScheduleIO1
{
    public partial class Form2 : Form
    {

        public string db;
        
        public Form2(string login)
        {
            InitializeComponent();
            button_customuze();
            Statusline.Left = ClientSize.Width / 2 - Statusline.Width / 2;
            Thread checker = new Thread(internet_check);
            checker.Start();
            db = login;
            main_info();
        }

        public void internet_check()
        {
            Thread.Sleep(2500);
            while (true)
            {
                

                string strServer = "http://www.schedule-io.tk/";
                try
                {
                    HttpWebRequest reqFP = (HttpWebRequest)HttpWebRequest.Create(strServer);
                    HttpWebResponse rspFP = (HttpWebResponse)reqFP.GetResponse();
                    if (HttpStatusCode.OK == rspFP.StatusCode)
                    {
                        // HTTP = 200 - Интернет безусловно есть!
                        rspFP.Close();
                        pictureBox1.BackColor = Color.FromArgb(139, 195, 74);

                        this.Invoke(new Action(() =>
                        {
                            Statusline.Text = "Connected";
                            Statusline.BackColor = Color.FromArgb(139, 195, 74);
                            Statusline.Left = ClientSize.Width / 2 - Statusline.Width / 2;
                        })); 

                    }
                    else
                    {
                        // сервер вернул отрицательный ответ, возможно что инета нет
                        rspFP.Close();
                        pictureBox1.BackColor = Color.Red;
                    }

                }
                catch (WebException)
                {
                    pictureBox1.BackColor = Color.FromArgb(244, 67, 54);
                    this.Invoke(new Action(() =>
                    {
                        Statusline.Text = "Connection failed";
                        Statusline.BackColor = Color.FromArgb(244, 67, 54);
                        Statusline.Left = ClientSize.Width / 2 - Statusline.Width / 2;
                    }));
                }
                Thread.Sleep(200);
            }
        }



        public void main_info()
        {
            MySqlConnection conn = DBProperty.GetDBConnection();
            try
            {
                conn.Open();
                string sql = "SELECT name FROM users WHERE login = '" + db + "'";
                MySqlCommand command = new MySqlCommand(sql, conn);
                string request = command.ExecuteScalar().ToString();
                string firstName = label1.Text;
                label1.Text = firstName + request;
                string sql2 = "SELECT surname FROM users WHERE login = '" + db + "'";
                MySqlCommand command2 = new MySqlCommand(sql2, conn);
                string request2 = command2.ExecuteScalar().ToString();
                string lastName = label2.Text;
                label2.Text = lastName + request2;
            }
            catch (Exception er)
            {
                //MessageBox.Show("Error: "+er.Message,"Schedule");
            }
        }

        

        private void label2_Click(object sender, EventArgs e)
        {

        }
        public static Form3 form3;

        private void button1_Click(object sender, EventArgs e)
        {
            form3 = new Form3();
            form3.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void auth_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            Form3 qwer = new Form3();
            qwer.BackColor = Color.Aquamarine;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Night_mode modeCheck = new Night_mode();
            modeCheck.NgChecker("Main");
        }
        private void button_customuze()
        {
            auth.FlatAppearance.BorderSize = 0;
            auth.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            button2.FlatStyle = FlatStyle.Flat;
            button3.FlatAppearance.BorderSize = 0;
            button3.FlatStyle = FlatStyle.Flat;
        }
    }
}

