﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using ScheduleIO1.Properties;
namespace ScheduleIO1
{
    class Night_mode
    {
        public void NgChecker(string window)
        {
            string lel = Properties.Settings.Default.NgMode;
            if (lel == "Night")
            {
                enableNightMode(window);
            }
            else
            {
                enableLightMode(window);
            }
        }
        public void enableNightMode(string window)
        {
            //Main form
            if (window=="Main")
                Form1.form2.BackColor = Color.FromArgb(51, 72, 83);
                Form1.form2.auth.BackColor = Color.FromArgb(38, 50, 56);
                Form1.form2.button1.BackColor = Color.FromArgb(38, 50, 56);
                Form1.form2.button2.BackColor = Color.FromArgb(38, 50, 56);
                Form1.form2.button3.BackColor = Color.FromArgb(38, 50, 56);
                Form1.form2.panel1.BackColor = Color.FromArgb(51, 72, 83);
                Form1.form2.label1.ForeColor = Color.FromArgb(255, 255, 255);
                Form1.form2.label2.ForeColor = Color.FromArgb(255, 255, 255);
                Form1.form2.label3.ForeColor = Color.FromArgb(255, 255, 255);
                Form1.form2.label4.ForeColor = Color.FromArgb(255, 255, 255);
            //Main form
            if (window == "Settings")
                Form2.form3.BackColor = Color.FromArgb(51, 72, 83);
        }
        public void enableLightMode(string window)
        {
            //Auth form
            if (window == "Main")
                Form1.form2.BackColor = Color.FromArgb(236, 240, 241);
                Form1.form2.auth.BackColor = Color.FromArgb(189, 195, 199);
                Form1.form2.button1.BackColor = Color.FromArgb(189, 195, 199);
                Form1.form2.button2.BackColor = Color.FromArgb(189, 195, 199);
                Form1.form2.button3.BackColor = Color.FromArgb(189, 195, 199);
                Form1.form2.panel1.BackColor = Color.FromArgb(236, 240, 241);
                Form1.form2.label1.ForeColor = Color.FromArgb(51, 72, 83);
                Form1.form2.label2.ForeColor = Color.FromArgb(51, 72, 83);
                Form1.form2.label3.ForeColor = Color.FromArgb(51, 72, 83);
                Form1.form2.label4.ForeColor = Color.FromArgb(51, 72, 83);
            //Main form
            if (window == "Settings")
                Form2.form3.BackColor = Color.FromArgb(236, 240, 241);
        }
    }
}
