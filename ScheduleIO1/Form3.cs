﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Tutorial.SqlConn;
using ScheduleIO1.Properties;

namespace ScheduleIO1
{
    public partial class Form3 : Form
    {

        public string new_password;
        public Form3()
        {

            InitializeComponent();
            textBox1.Left = ClientSize.Width / 2 - textBox1.Width / 2;
            textBox2.Left = ClientSize.Width / 2 - textBox2.Width / 2;
            button1.Left = ClientSize.Width / 2 - button1.Width / 2;
 
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            textBox1.Text = "Password";
            textBox2.Text = "New password";
            Night_mode modeCheck = new Night_mode();
            modeCheck.NgChecker("Settings");
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string login = Form1.login;
            string password = textBox1.Text;
            if(password != Form1.password)
            {
                MessageBox.Show("Incorrect password");
            }
            else
            {
                new_password = textBox2.Text;
                MySqlConnection conn = DBProperty.GetDBConnection();
                conn.Open();
                string sql = "UPDATE users SET password = '" + new_password + "' WHERE login ='" + login + "'";
                MySqlCommand command = new MySqlCommand(sql, conn);
                command.ExecuteScalar();
                MessageBox.Show("Success");
                this.Close();
            }
        }


        

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Password")
            {
                textBox1.Text = "";
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "Password";
                {
                }
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "New password")
            {
                textBox2.Text = "";
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "New password";
                {
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.Text == "Black")
            {
                Properties.Settings.Default.NgMode = "Night";
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
                Night_mode modeCheck = new Night_mode();
                modeCheck.NgChecker("Main");
                modeCheck.NgChecker("Settings");
            }

            if(comboBox1.Text == "White")
            {
                Properties.Settings.Default.NgMode = "Light";
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
                Night_mode modeCheck = new Night_mode();
                modeCheck.NgChecker("Main");
                modeCheck.NgChecker("Settings");
            }
        }
    }
}
